import psycopg2

def makeTables(cur):
    cur.execute(""\
        "BEGIN; "\
        "CREATE TABLE visitor(name VARCHAR, time INT default 0); "\
        "COMMIT;")

def connect():
    try:
        connect_str = "dbname='ubuntu' user='wego' host='localhost' password='wego'"
        conn = psycopg2.connect(connect_str)
        return conn
    except:
        pass

def main():
    conn = connect()
    assert conn != None, "Could not connect to database"
    cur = conn.cursor()
    makeTables(cur)

if __name__ == '__main__': main()

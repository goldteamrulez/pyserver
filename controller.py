import _visitor
import _dbaccess
from endpoints import Controller

class Default(Controller):
  def GET(self):
    return 'Hi!'

  def POST(self, **kwargs):
    ## Get input
    name = kwargs['name']

    response = "Hello {0}!".format(name)
    return response

class Visit(Controller, _visitor.funs, _dbaccess.funs):
  def GET(self): 
    response = self.visit_page()
    return response

class Info(Controller, _visitor.funs):
  def GET(self): 
    response = self.get_info()
    return response

class Example(Controller, _visitor.funs):
  def GET(self): 
    response = self.get_example()
    return response

import time

class funs():
  def visit_page(self):
    ## make sure that there is a connection ready
    self.prepare_connection()
    cursor = self.cur

    ## Add visit to records
    timestamp = int(time.time())
    data = {'visitor':'Visitor', 'time':timestamp}
    self.add_visitor_record(data,cursor)

    ## Generate response
    count = self.get_visitor_count(cursor)
    response = 'Hello visitor #{0}!'.format(count)

    return response

  def POST(self, **kwargs):
    ## Get input
    name = kwargs['name']

    ## make sure that there is a connection ready
    self.prepare_connection()
    cursor = self.cur

    ## Add visit to records
    timestamp = int(time.time())
    data = {'visitor':name, 'time':timestamp}
    self.add_visitor_record(data,cursor)

    ## Generate response
    visitors = self.get_visitors(cursor)
    response = 'Hello {0}! '\
               'Other people who have visited: '\
               '{1}'.format(name, visitors)

    return response

  def get_visitor_count(self,cursor):
    cursor.execute("SELECT count(name) FROM visitor;")
    visitorcount = int(cursor.fetchall()[0][0])
    return visitorcount

  def get_visitors(self,cursor):
    visitors = []
    cursor.execute("SELECT name FROM visitor;")
    for v in cursor.fetchall():
      visitors.append(v[0])
    return visitors

  def add_visitor_record(self,data,cursor):
    command = "INSERT INTO visitor(name,time) VALUES ('{visitor}',{time}) ON CONFLICT DO NOTHING; COMMIT;".format(**data)
    cursor.execute(command)

  def get_info(self):
    info = 'You can visit generically by sending a GET request or you can record a name '\
           'for your visit by specifying a parameter \'name\' with your name. '\
           'Visit http://35.165.191.227:8080/example for a sample to run in browser.'
    return info

  def get_example(self):
    example = 'data:text/html,<body onload="document.body.firstChild.submit()">'\
           '<form method="post" action="http://35.165.191.227:8080/visit">'\
           '<input value="Roman" name="name">'
    return example
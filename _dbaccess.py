import psycopg2

class funs():
  def prepare_connection(self):
    ## Verify DB Connection
    if 'cur' not in locals():
      self.init_pool()
      print ('Initialized connection to DB')

  def init_pool(self):
    self.connected = False
    self.conn = self.connect()
    assert self.conn != None, "Could not connect to database"
    self.cur = self.conn.cursor()
    self.connected = True

  def connect(self):
    try:
        connect_str = "dbname='ubuntu' user='wego' host='localhost' password='wego'"
        conn = psycopg2.connect(connect_str)
        return conn
    except:
	print('Failed to connect')
        pass